# Geospatial Tools 

These tools automate processes related to the building and updating of FrameFinder's underlying air photo centerpoints dataset, and to the creation of GIS map indexes.

