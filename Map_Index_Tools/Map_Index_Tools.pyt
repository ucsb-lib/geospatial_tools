import arcpy
import os

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the .pyt file)."""
        self.label = ""
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [Add_Index_Fields, Add_Extent_Fields]


class Add_Index_Fields(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Add_Index_Fields"
        self.description = "Use this tool to add the following fields to a GIS index: label, title, date, datePub, dateReprnt, edition, available, inst, physHold, digHold, fileName, fileName1, fileName2.  Do not run this tool on a FC that already contains any of these field names!"
        self.canRunInBackground = False

    def getParameterInfo(self):
	"""Define parameter definitions"""

     	param0 = arcpy.Parameter(
         	displayName="Shapefile/Feature Class",
         	name="input_fc",
         	datatype="feature class",
         	parameterType="Required",
         	direction="Input")

 	params = [param0]

        return params

    def execute(self, parameters, messages):
        """The source code of the tool."""
 
 	try:
  		polygonFC = parameters[0].valueAsText

		# Set local variables
		fieldName1 = "label"
		fieldLength1 = 50
		fieldName2 = "title"
		fieldLength2 = 50
		fieldName3 = "date"
		fieldLength3 = 25
		fieldName3A = "datePub"
		fieldLength3A = 25
		fieldName3B = "dateReprnt"
		fieldLength3B = 25
		fieldName4 = "edition"
		fieldLength4 = 25
		fieldName5 = "available"
		fieldLength5 = 10
		fieldName6 = "inst"
		fieldLength6 = 50
		fieldName7 = "physHold"
		fieldLength7 = 250
		fieldName8 = "digHold"
		fieldLength8 = 250
		fieldName9 = "fileName"
		fieldLength9 = 250
		fieldName10 = "fileName1"
		fieldLength10 = 250
		fieldName11 = "fileName2"
		fieldLength11 = 250
 
		arcpy.AddField_management(polygonFC, fieldName1, "TEXT", field_length=fieldLength1)
		arcpy.AddField_management(polygonFC, fieldName2, "TEXT", field_length=fieldLength2)
		arcpy.AddField_management(polygonFC, fieldName3, "TEXT", field_length=fieldLength3)
		arcpy.AddField_management(polygonFC, fieldName3A, "TEXT", field_length=fieldLength3A)
		arcpy.AddField_management(polygonFC, fieldName3B, "TEXT", field_length=fieldLength3B)
		arcpy.AddField_management(polygonFC, fieldName4, "TEXT", field_length=fieldLength4)
		arcpy.AddField_management(polygonFC, fieldName5, "TEXT", field_length=fieldLength5)
		arcpy.AddField_management(polygonFC, fieldName6, "TEXT", field_length=fieldLength6)
		arcpy.AddField_management(polygonFC, fieldName7, "TEXT", field_length=fieldLength7)
		arcpy.AddField_management(polygonFC, fieldName8, "TEXT", field_length=fieldLength8)
		arcpy.AddField_management(polygonFC, fieldName9, "TEXT", field_length=fieldLength9)
		arcpy.AddField_management(polygonFC, fieldName10, "TEXT", field_length=fieldLength10)
		arcpy.AddField_management(polygonFC, fieldName11, "TEXT", field_length=fieldLength11)

		arcpy.CalculateField_management(polygonFC, fieldName6, "\"UC Santa Barbara\"")
		
  		del polygonFC

 	except:
      		arcpy.AddMessage(arcpy.GetMessages(2))
      		print (arcpy.GetMessages(2))
  		raise arcpy.ExecuteError
        return

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


class Add_Extent_Fields(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Add_Extent_Fields"
        self.description = "This tool reprojects input FC to WGS1984, then adds polygon extent fields and renames them to 'west', 'east', 'north', 'south'. The original FC is saved with suffix '_OLD'. Do not run this tool on a FC that already contains these field names!"
        self.canRunInBackground = False

    def getParameterInfo(self):
	"""Define parameter definitions"""

	"""
     	param0 = arcpy.Parameter(
         	displayName="Workspace GDB",
         	name="input_ws",
         	datatype="DEWorkspace",
         	parameterType="Optional",
         	direction="Input")
	"""

     	param0 = arcpy.Parameter(
         	displayName="Feature Class",
         	name="input_fc",
         	datatype="feature class",
         	parameterType="Required",
         	direction="Input")
		
 	params = [param0]

        return params

    def execute(self, parameters, messages):
        """The source code of the tool."""
 
 	try:
  		#arcpy.env.workspace = parameters[0].valueAsText    
		arcpy.env.overwriteOutput = True
  		polygonFC = parameters[0].valueAsText

		outputFC = polygonFC + "_OLD"
		fileName = os.path.basename(polygonFC)
		dirPath = os.path.dirname(polygonFC)

		arcpy.Rename_management(polygonFC, outputFC)
		out_coordinate_system = arcpy.SpatialReference(4326)
		arcpy.Project_management(outputFC, polygonFC, out_coordinate_system)
		
		arcpy.AddGeometryAttributes_management(polygonFC, "EXTENT")
		arcpy.AlterField_management(polygonFC, 'EXT_MIN_X', 'west')
		arcpy.AlterField_management(polygonFC, 'EXT_MAX_X', 'east')
		arcpy.AlterField_management(polygonFC, 'EXT_MAX_Y', 'north')
		arcpy.AlterField_management(polygonFC, 'EXT_MIN_Y', 'south')

  		del polygonFC
  		del outputFC

 	except:
      		arcpy.AddMessage(arcpy.GetMessages(2))
      		print (arcpy.GetMessages(2))
  		raise arcpy.ExecuteError
        return

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

