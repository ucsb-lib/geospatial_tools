import arcpy
import os
import traceback

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the .pyt file)."""
        self.label = ""
        self.alias = ""

        # List of tool classes associated with this toolbox
        # self.tools = [Append_Shapefiles, Update_Digital_Holdings, Find_Duplicates, Shapefiles_To_GDB, Flightprint_To_JPEG, Count_Workspace_Records]
        self.tools = [Append_Shapefile, Update_Digital_Holdings, Validate_Shapefile]


class Append_Shapefile(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "2_Append_Shapefile"
        self.description = "For a user selected shapefile, deletes any records for the flight already in the selected target feature class, then adds records from the shapefile to the target feature class."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""

	# First parameter
    	param0 = arcpy.Parameter(
        	displayName="Source Shapefile",
        	name="in_source",
        	datatype="DEShapefile",
        	parameterType="Required",
        	direction="Input")

	# Second parameter
    	param1 = arcpy.Parameter(
        	displayName="Target Feature Class",
        	name="out_target",
        	datatype="feature class",
        	parameterType="Required",
        	direction="Input")

	# Third parameter
    	param2 = arcpy.Parameter(
        	displayName="Expanded Holdings - Integration Flight",
        	name="out_exphold",
        	datatype="boolean",
        	parameterType="Optional",
        	direction="Input")

	# Fourth parameter
    	param3 = arcpy.Parameter(
        	displayName="Lat Long Flight",
        	name="out_latlong",
        	datatype="boolean",
        	parameterType="Optional",
        	direction="Input")

	params = [param0, param1, param2, param3]
	return params

    def execute(self, parameters, messages):
        """The source code of the tool."""

	# Set the source shapefile to value of first input parameter
	sf = parameters[0].valueAsText

	# Set the target feature class to value of second input parameter
	target = parameters[1].valueAsText

	# Check if this is a latlong flight
	latlong_flight = 0
	if parameters[3].valueAsText: latlong_flight = 1

	expandedHoldings = False
	sourceFields = ['SHAPE@', 'Frame', 'Held', 'FlightID', 'Scale', 'BeginDate']
	targetFields = ['SHAPE@', 'Frame', 'Held', 'FlightID', 'Scale', 'BeginDate', 'LatLongFlight', 'Scan']
	
	#if arcpy.ListFields(sf, "Roll"):
	# Check if the flight has expanded holdings
	if parameters[2].valueAsText:
		# Check if the source shapefile has a 'Roll' field (contains expanded holdings info)
		if arcpy.ListFields(sf, "Roll"):
			expandedHoldings = True
			sourceFields = ['SHAPE@', 'Frame', 'Held', 'FlightID', 'Scale', 'BeginDate', 'Roll', 'Nitrate', 'Cut_Frame', 'Print']
			targetFields = ['SHAPE@', 'Frame', 'Held', 'FlightID', 'Scale', 'BeginDate', 'LatLongFlight', 'Scan', 'Roll', 'Nitrate', 'Cut_Frame', 'Print']
		else:
			arcpy.AddMessage("NO EXPANDED HOLDINGS INFO IN SHAPEFILE")

	try:		
		#arcpy.AddMessage("TEST 1")
		source_cursor = arcpy.da.SearchCursor(sf, sourceFields)
		flightID = source_cursor.next()[3].strip()
		source_cursor.reset()
		arcpy.AddMessage("FlightID: " + flightID)
		flight_frame_count = 0
	
		# search for flight in target feature class
		
		Query = '"FlightID"='+ "'" + flightID + "'"
		delete_cursor = arcpy.da.UpdateCursor(target, ["*"], Query)

		# delete selected flight records out of the target feature class
		deleted_frame_count = 0
		for row in delete_cursor:
			delete_cursor.deleteRow()
			deleted_frame_count += 1

		arcpy.AddMessage(str(deleted_frame_count) + " records deleted")
		del delete_cursor
		
		#targetFields = ['SHAPE@', 'Frame', 'Held', 'FlightID', 'Scale', 'BeginDate', 'LatLongFlight', 'Scan', 'Roll', 'Nitrate', 'Cut_Frame', 'Print']
		target_cursor = arcpy.da.InsertCursor(target, targetFields)

		with source_cursor:
			for source_row in source_cursor:
				frame = source_row[1].strip()
				scanFlightID = flightID.replace("_", "-").lower()
				scanName = scanFlightID + "_" + frame.lower()
				scanName = scanName.replace(" ", "")
				
				heldValue = source_row[2].strip()
				scaleValue = None
				if source_row[4] > 0:
					scaleValue = source_row[4]

				scanPath = r"//svmwindows/mil/airphotos/airphotos-new/" + scanFlightID + "/" + scanName + ".tif"
				if os.path.isfile(scanPath):
					heldValue = "YES"
					#scan = (r"<a href=https://mil.library.ucsb.edu/ap_images/" + scanFlightID + "/" + scanFlightID + "_" + scanFrame + r".tif>Free Download</a>")
					scan = r'<a data-on="click" data-event-category="imagery" data-event-action="click download link" data-event-label="' + scanName + r'.tif" target="_blank" href=https://mil.library.ucsb.edu/ap_images/' + scanFlightID + r'/' + scanName + r'.tif>Free Download</a>'

				else:
					#scan = r'<a href=https://researchspecial.library.ucsb.edu/logon?Action=10&Form=30&genre=AirPhoto target = "_blank">Initiate Purchase</a>'
					scan = r'<a data-on="click" data-event-category="imagery" data-event-action="click scan request" data-event-label="' + scanName + r'" target="_blank" href=https://researchspecial.library.ucsb.edu/logon?Action=10&Form=30&genre=AirPhoto>Initiate Purchase</a>'
						
				scanPath = ""

				if expandedHoldings:
					target_cursor.insertRow((source_row[0], frame, heldValue, flightID, scaleValue, source_row[5], latlong_flight, scan, source_row[6], source_row[7], source_row[8], source_row[9]))
				else:
					target_cursor.insertRow((source_row[0], frame, heldValue, flightID, scaleValue, source_row[5], latlong_flight, scan))
				
				flight_frame_count += 1 

		arcpy.AddMessage(str(flight_frame_count) + " records appended")
		del target_cursor
		del source_cursor
		del sf
		del target
		
	except Exception as err:
      		arcpy.AddMessage(arcpy.GetMessages(2))
      		print (arcpy.GetMessages(2))
		arcpy.AddMessage(traceback.format_exc())
  		raise arcpy.ExecuteError
        return

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

####################################################################################################################################################################################################
####################################################################################################################################################################################################

class Update_Digital_Holdings(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "3_Update_Digital_Holdings"
        self.description = "Checks for the existence of a digital air photo for each record in a shapefile/feature class.  If the Scan value contains the word 'researchspecial' (found in the scan request path), sets the value to the file path if a digital photo is found."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""

	# First parameter
    	param0 = arcpy.Parameter(
        	displayName="Shapefile/Feature Class",
        	name="input_fc",
        	datatype="feature class",
        	parameterType="Required",
        	direction="Input")

	params = [param0]
	return params

    def execute(self, parameters, messages):
        """The source code of the tool."""

	# Set feature class to the value of the input parameter
	#
	fc = parameters[0].valueAsText
	fields = ['FlightID', 'Frame', 'Scan', 'Held']
	try:
		with arcpy.da.UpdateCursor(fc, fields) as rows:
			record_update_count = 0

			for row in rows:
				flightID = row[0].replace("_", "-").strip().lower()
				frame = row[1].strip().lower()
				scanName = flightID + "_" + frame
				scanName = scanName.replace(" ", "")
				if row[2] <> None:
					if 'researchspecial' in row[2]:
						scanPath = r"//svmwindows/mil/airphotos/airphotos-new/" + flightID + "/" + scanName + ".tif"
						if os.path.isfile(scanPath):
							#row[2] = r'<a href=https://mil.library.ucsb.edu/ap_images/' + flightID + r'/' + scanName + r'.tif>Free Download</a>'
							row[2] = r'<a data-on="click" data-event-category="imagery" data-event-action="click download link" data-event-label="' + scanName + r'.tif" target="_blank" href=https://mil.library.ucsb.edu/ap_images/' + flightID + r'/' + scanName + r'.tif>Free Download</a>'
							row[3] = "YES"
							rows.updateRow(row)
							record_update_count += 1
						scanPath = ""
				else:
					#row[2] = r'<a href=https://researchspecial.library.ucsb.edu/logon?Action=10&Form=30&genre=AirPhoto target = "_blank">Initiate Purchase</a>'
					row[2] = r'<a data-on="click" data-event-category="imagery" data-event-action="click scan request" data-event-label="' + scanName + r'" target="_blank" href=https://researchspecial.library.ucsb.edu/logon?Action=10&Form=30&genre=AirPhoto>Initiate Purchase</a>'
					rows.updateRow(row)

		arcpy.AddMessage(str(record_update_count) + " records updated")
		del rows
		del row
		del fc
		
	except Exception as err:
      		arcpy.AddMessage(arcpy.GetMessages(2))
      		print (arcpy.GetMessages(2))
		arcpy.AddMessage(traceback.format_exc())
  		raise arcpy.ExecuteError
        return

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


####################################################################################################################################################################################################
####################################################################################################################################################################################################

class Validate_Shapefile(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "1_Validate_Shapefile"
        self.description = "Identifies duplicate frames in a shapefile/feature class, and identifies records which contain space characters in the FlightID or frame number."
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""

	# First parameter
    	param0 = arcpy.Parameter(
        	displayName="Shapefile/Feature Class",
        	name="input_fc",
        	datatype="feature class",
        	parameterType="Required",
        	direction="Input")

	params = [param0]

        return params

    def execute(self, parameters, messages):
        """The source code of the tool."""

	# Set feature class to the value of the input parameter
	#
	fc = parameters[0].valueAsText
	fields = ['FlightID', 'Frame']

	try:
		cursor1 = arcpy.da.SearchCursor(fc, fields)
		dupesFound = 0

		with cursor1:
			for cursor1_row in cursor1:
				#search for duplicates
				Query = '"FlightID"=' + "'" + cursor1_row[0] + "'" + ' AND ' + '"Frame"=' + "'" + cursor1_row[1] + "'"
				cursor2 = arcpy.da.SearchCursor(fc, fields, Query)
				count = 0

				with cursor2:
					for cursor2_row in cursor2:
						count += 1
					
				if count > 1:
					arcpy.AddMessage("Flight " + cursor1_row[0] + ", Frame " + cursor1_row[1] + " is not unique")
					dupesFound = 1
				
				if (cursor1_row[0].find(' ') != -1): arcpy.AddMessage("FlightID for " + cursor1_row[0] + ", Frame " + cursor1_row[1] + " contains a space character")
				if (cursor1_row[1].find(' ') != -1): arcpy.AddMessage("Frame for " + cursor1_row[0] + ", Frame " + cursor1_row[1] + " contains a space character")
				
		del cursor1
		del cursor2
		del fc
		if dupesFound == 0:
			arcpy.AddMessage("No duplicates found.")
		
	except Exception as err:
      		arcpy.AddMessage(arcpy.GetMessages(2))
      		print (arcpy.GetMessages(2))
		arcpy.AddMessage(traceback.format_exc())
  		raise arcpy.ExecuteError
        return

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return
